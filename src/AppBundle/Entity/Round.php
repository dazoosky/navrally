<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Round
 *
 * @ORM\Table(name="round")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RoundRepository")
 */
class Round
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="number", type="integer")
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="customName", type="string", length=255, nullable=true)
     */
    private $customName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="additionalReq", type="text", nullable=true)
     */
    private $additionalReq;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="eventDate", type="datetime")
     */
    private $eventDate;

    /**
     * @var int
     *
     * @ORM\Column(name="maxParticipants", type="integer")
     */
    private $maxParticipants;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="signUp_start", type="datetime")
     */
    private $signUpStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="signUp_stop", type="datetime")
     */
    private $signUpStop;


    /**
     * @var
     * @ORM\ManyToOne(targetEntity="league")
     * @ORM\JoinColumn(name="league_id", referencedColumnName="id")
     */
    private $league;

    /**
     * @return mixed
     */
    public function getLeague()
    {
        return $this->league;
    }

    /**
     * @param mixed $league
     */
    public function setLeague($league)
    {
        $this->league = $league;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Round
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Round
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set customName
     *
     * @param string $customName
     *
     * @return Round
     */
    public function setCustomName($customName)
    {
        $this->customName = $customName;

        return $this;
    }

    /**
     * Get customName
     *
     * @return string
     */
    public function getCustomName()
    {
        return $this->customName;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Round
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set additionalReq
     *
     * @param string $additionalReq
     *
     * @return Round
     */
    public function setAdditionalReq($additionalReq)
    {
        $this->additionalReq = $additionalReq;

        return $this;
    }

    /**
     * Get additionalReq
     *
     * @return string
     */
    public function getAdditionalReq()
    {
        return $this->additionalReq;
    }

    /**
     * Set eventDate
     *
     * @param \DateTime $eventDate
     *
     * @return Round
     */
    public function setEventDate($eventDate)
    {
        $this->eventDate = $eventDate;

        return $this;
    }

    /**
     * Get eventDate
     *
     * @return \DateTime
     */
    public function getEventDate()
    {
        return $this->eventDate;
    }

    /**
     * Set maxParticipants
     *
     * @param integer $maxParticipants
     *
     * @return Round
     */
    public function setMaxParticipants($maxParticipants)
    {
        $this->maxParticipants = $maxParticipants;

        return $this;
    }

    /**
     * Get maxParticipants
     *
     * @return int
     */
    public function getMaxParticipants()
    {
        return $this->maxParticipants;
    }

    /**
     * Set signUpStart
     *
     * @param \DateTime $signUpStart
     *
     * @return Round
     */
    public function setSignUpStart($signUpStart)
    {
        $this->signUpStart = $signUpStart;

        return $this;
    }

    /**
     * Get signUpStart
     *
     * @return \DateTime
     */
    public function getSignUpStart()
    {
        return $this->signUpStart;
    }

    /**
     * Set signUpStop
     *
     * @param \DateTime $signUpStop
     *
     * @return Round
     */
    public function setSignUpStop($signUpStop)
    {
        $this->signUpStop = $signUpStop;

        return $this;
    }

    /**
     * Get signUpStop
     *
     * @return \DateTime
     */
    public function getSignUpStop()
    {
        return $this->signUpStop;
    }
}

