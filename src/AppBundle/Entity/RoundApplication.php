<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RoundApplication
 *
 * @ORM\Table(name="round_application")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RoundApplicationRepository")
 */
class RoundApplication
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creationDate", type="datetime")
     */
    private $creationDate;

    /**
     * @var string
     *
     * @ORM\Column(name="additionalInfo", type="string", length=255, nullable=true)
     */
    private $additionalInfo;


    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="roundApplications")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @ORM\ManyToOne(targetEntity="LeagueApplication", inversedBy="roundApplications")
     * @ORM\JoinColumn(name="leagueApplication_id", referencedColumnName="id")
     */
    private $leagueApplication;

    /**
     * @ORM\ManyToOne(targetEntity="RoundApplicationStatus")
     * @ORM\JoinColumn(name="status", referencedColumnName="status")
     */
    private $status;

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return mixed
     */
    public function getLeagueApplication()
    {
        return $this->leagueApplication;
    }

    /**
     * @param mixed $leagueApplication
     */
    public function setLeagueApplication($leagueApplication)
    {
        $this->leagueApplication = $leagueApplication;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     *
     * @return RoundApplication
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set additionalInfo
     *
     * @param string $additionalInfo
     *
     * @return RoundApplication
     */
    public function setAdditionalInfo($additionalInfo)
    {
        $this->additionalInfo = $additionalInfo;

        return $this;
    }

    /**
     * Get additionalInfo
     *
     * @return string
     */
    public function getAdditionalInfo()
    {
        return $this->additionalInfo;
    }
}

