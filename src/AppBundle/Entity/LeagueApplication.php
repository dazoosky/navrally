<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * leagueApplication
 *
 * @ORM\Table(name="league_application")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\leagueApplicationRepository")
 */
class LeagueApplication
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creationDate", type="datetime")
     */
    private $creationDate;

    /**
     * @var int
     *
     * @ORM\Column(name="startNo", type="integer", nullable=true)
     */
    private $startNo;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="leagueApplications")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @ORM\ManyToOne(targetEntity="League", inversedBy="leagueApplications")
     * @ORM\JoinColumn(name="league_id", referencedColumnName="id")
     */
    private $league;

    /**
     * @ORM\ManyToOne(targetEntity="LeagueApplicationStatus")
     * @ORM\JoinColumn(name="status", referencedColumnName="status")
     */
    private $status;


    /**
     * @ORM\OneToMany(targetEntity="RoundApplication", mappedBy="leagueApplication")
     */
    private $roundApplications;

    public function __construct()
    {
        $this->roundApplications = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getRoundApplications()
    {
        return $this->roundApplications;
    }

    /**
     * @param mixed $roundApplications
     */
    public function setRoundApplications($roundApplications)
    {
        $this->roundApplications = $roundApplications;
    }



    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getLeague()
    {
        return $this->league;
    }

    /**
     * @param mixed $league
     */
    public function setLeague($league)
    {
        $this->league = $league;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     *
     * @return LeagueApplication
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set startNo
     *
     * @param integer $startNo
     *
     * @return LeagueApplication
     */
    public function setStartNo($startNo)
    {
        $this->startNo = $startNo;

        return $this;
    }

    /**
     * Get startNo
     *
     * @return int
     */
    public function getStartNo()
    {
        return $this->startNo;
    }
}

