<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="members")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     */
    private $team;

    /**
     * @ORM\ManyToOne(targetEntity="UserType", inversedBy="users")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="League", mappedBy="owner")
     */
    private $leagues;

    /**
     * @ORM\OneToMany(targetEntity="LeagueApplication", mappedBy="owner")
     */
    private $leagueApplications;

    /**
     * @ORM\OneToMany(targetEntity="RoundApplication", mappedBy="owner");
     */
    private $roundApplications;


    public function __construct()
    {
        $this->leagues = new ArrayCollection();
        $this->leagueApplications = new ArrayCollection();
        $this->roundApplications = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getRoundApplications()
    {
        return $this->roundApplications;
    }

    /**
     * @param mixed $roundApplications
     */
    public function setRoundApplications($roundApplications)
    {
        $this->roundApplications = $roundApplications;
    }

    /**
     * @return mixed
     */
    public function getLeagueApplications()
    {
        return $this->leagueApplications;
    }

    /**
     * @param mixed $leagueApplications
     */
    public function setLeagueApplications($leagueApplications)
    {
        $this->leagueApplications = $leagueApplications;
    }

    /**
     * @return mixed
     */
    public function getLeagues()
    {
        return $this->leagues;
    }

    /**
     * @param mixed $leagues
     */
    public function setLeagues($leagues)
    {
        $this->leagues = $leagues;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @param mixed $team
     */
    public function setTeam($team)
    {
        $this->team = $team;
    }

//    /**
//     * User constructor.
//     */
//    public function __construct()
//    {
//        parent::__construct();
//        // your own logic
//    }


}