<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * LeagueCategory
 *
 * @ORM\Table(name="league_category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LeagueCategoryRepository")
 */
class LeagueCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="requirements", type="text", nullable=true)
     */
    private $requirements;

    /**
     * @return mixed
     */
    public function getLeagues()
    {
        return $this->leagues;
    }

    /**
     * @param mixed $leagues
     */
    public function setLeagues($leagues)
    {
        $this->leagues = $leagues;
    }



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return LeagueCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set requirements
     *
     * @param string $requirements
     *
     * @return LeagueCategory
     */
    public function setRequirements($requirements)
    {
        $this->requirements = $requirements;

        return $this;
    }

    /**
     * Get requirements
     *
     * @return string
     */
    public function getRequirements()
    {
        return $this->requirements;
    }
}

